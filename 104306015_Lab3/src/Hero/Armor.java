package Hero;

/*
 *  One thing needs be done in line 4
 */
public class Armor implements Equipment {

	private final double buff = 1.5;
	private final String name = "Armor";

	public void buff(Superhero hero) {
		hero.setDefense(hero.getDefense() * buff);
	}

	public void debuff(Superhero hero) {
		hero.setDefense(hero.getDefense() / buff);
	}

	public boolean isSuitable(Superhero hero) {
		return true;
	}

	public String getName() {
		return name;
	}
}
