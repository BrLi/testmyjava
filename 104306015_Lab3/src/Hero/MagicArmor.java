package Hero;

/*
 *  One thing needs be done in line 4
 */
public class MagicArmor implements Equipment {

	private final String name = "Invisibility cloak";
	private double originalDefense;

	public void buff(Superhero hero) {
		this.originalDefense = hero.getDefense();
		hero.setDefense(1);
	}

	public void debuff(Superhero hero) {
		hero.setDefense(originalDefense);
	}

	public boolean isSuitable(Superhero hero) {
		boolean s;
		if (hero instanceof Magician) {
			s = true;
		} else {
			s = false;
		}
		return s;
	}

	public String getName() {
		return name;
	}

}
