package Hero;

public class Superhero implements Speaking {
	private String name;
	private double healthPoint;
	private double damage;
	private Equipment equipment = null;
	private double defense;

	public Superhero(String name, double healthPoint, double damage, double defense) {
		this.setName(name);
		this.setHealthPoint(healthPoint);
		this.setDamage(damage);
		this.setDefense(defense);
	}

	// ----New Part----//
	/**
	 * set the equipment for the superhero, and check whether the superhero can use
	 * the given equipment. If not, show message like "Professor X cannot use Mace."
	 * 
	 * @param equipment equipment
	 */
	public void setEquipment(Equipment equipment) {
		if (equipment.isSuitable(this)) {
			this.equipment = equipment;
		} else {
			System.out.println(this.name + " cannot use " + equipment.getName());
		}
	}

	/**
	 * Get the name of the equipment the superhero using. If the superhero doesn't
	 * have any equipment, return "No equipment"
	 * 
	 * @return the name of the equipment
	 */
	public String getEquipment() {
		String s = null;
		if (this.equipment != null) {
			s = this.equipment.getName();
		} else {
			s = "No equipment";
		}
		return s;
	}

	/**
	 * The superhero is attacked by other, thus causing damage to the healthPoint.
	 * Hopefully, the superhero has the ability to defense, and reduce the damage.
	 * That is, the final damage equals to atk * (1 - defense).
	 * 
	 * @param atk damage from other superhero
	 */
	public void beAttacked(double atk) {
		if (this.healthPoint >= atk) {
			this.healthPoint -= atk * (1 - defense);
		}
	}

	public void format() {
		System.out.printf("The name of the superhero is \" %s \"\n", this.getName());
		System.out.printf("Current HP is %.2f \n", this.getHealthPoint());
		System.out.printf("Damage: %.2f\n", this.getDamage());
		System.out.printf("Defense: %.2f\n", this.getDefense());
		System.out.printf("Using equipment: %s\n", this.getEquipment());
	}

	public void buff() {
		if (equipment != null) {
			equipment.buff(this);
		}
	}

	public void debuff() {
		if (equipment != null) {
			equipment.debuff(this);
		}
	}

	public double getDefense() {
		return defense;
	}

	public void setDefense(double defense) {
		this.defense = defense;
	}

	public void fighting(Superhero anotherSuperhero) {
		Superhero thisSuperhero = this;
		System.out.println(getName() + " V.S. " + anotherSuperhero.getName() + "\n");
		System.out.println("Both Sides Using Equipment...");
		System.out.println();

		thisSuperhero.buff();
		anotherSuperhero.buff();

		System.out.println("Readly for fighting...");
		thisSuperhero.format();
		System.out.println();
		anotherSuperhero.format();

		thisSuperhero.attack(anotherSuperhero);
		anotherSuperhero.attack(thisSuperhero);
		System.out.println();

		System.out.println("Fighting end!");
		System.out.println("Unloading equipment...");
		thisSuperhero.debuff();
		anotherSuperhero.debuff();

		System.out.printf("%s's HP : %.2f\n", thisSuperhero.getName(), thisSuperhero.getHealthPoint());
		if (anotherSuperhero.getHealthPoint() <= 0) {
			System.out.println(anotherSuperhero.getName() + " is dead. ");
		} else {
			System.out.printf("%s's HP : %.2f\n", anotherSuperhero.getName(), anotherSuperhero.getHealthPoint());
		}
		System.out.println();
	}
	// ----Lab 1 & Lab 2----//

	public void attack(Superhero hero) {
		hero.beAttacked(this.getDamage());
	}

	public void greet() {
		System.out.println("Hi! I'm " + this.getName() + ".");
	}

	public void heal(double hp) {
		this.setHealthPoint(this.getHealthPoint() + hp);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getHealthPoint() {
		return healthPoint;
	}

	public void setHealthPoint(double healthPoint) {
		this.healthPoint = healthPoint;
	}

	public double getDamage() {
		return damage;
	}

	public void setDamage(double damage) {
		this.damage = damage;
	}
}
