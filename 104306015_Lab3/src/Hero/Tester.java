package Hero;

public class Tester {
	public static void main(String[] args) {

		Superhero hero1 = new Superhero("Superman", 820.0, 400.0, 0.07);

		Magician magician1 = new Magician("Professor X", 700.0, 300.0, 0, 1000.0, 350.0);

		Warrior warrior1 = new Warrior("Kamui", 800.0, 600.0, 0.05, "human", 1);

		hero1.setEquipment(new Armor()); // Armor
		magician1.setEquipment(new MagicArmor()); // MagicArmor
		warrior1.setEquipment(new MagicArmor()); // MagicArmor (Output: Kamui cannot use Invisibility cloak)
		warrior1.setEquipment(new Armor()); // Armor

		System.out.println("---Round 1---");
		hero1.fighting(magician1);
		System.out.println("---Round 2---");
		warrior1.fighting(hero1);

	}
}
