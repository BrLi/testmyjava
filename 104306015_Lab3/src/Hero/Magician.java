package Hero;

public class Magician extends Superhero {
	private double magicPoint;
	private double magicDamage;

	public Magician(String name, double hp, double ad, double defense, double mp, double md) {
		super(name, hp, ad, defense);
		this.magicPoint = mp;
		this.magicDamage = md;
	}

	public void attack(Superhero hero) {
		hero.beAttacked(magicDamage);
	}

	public void setMagicPoint(double mp) {
		this.magicPoint = mp;
	}

	public double getMagicPoint() {
		return this.magicPoint;
	}

	public void setMagicDamage(double md) {
		this.magicDamage = md;
	}

	public double getMagicDamage() {
		return this.magicDamage;
	}

	public void magicHeal(double mp) {
		this.magicPoint += mp;
	}

	@Override
	public void format() {
		System.out.printf("The name of the magician is \" %s \"\n", this.getName());
		System.out.printf("Current HP is %.2f \n", this.getHealthPoint());
		System.out.printf("Current MP is %.2f \n", this.getMagicPoint());
		System.out.printf("Damage: %.2f\n", this.getDamage());
		System.out.printf("Magic damage: %.2f\n", this.getMagicDamage());
		System.out.printf("Defense: %.2f\n", this.getDefense());
		System.out.printf("Using equipment: %s\n", this.getEquipment());
	}
}
