package Hero;

public interface Equipment {
	void buff(Superhero hero);

	void debuff(Superhero hero);

	boolean isSuitable(Superhero hero);

	String getName();
}
