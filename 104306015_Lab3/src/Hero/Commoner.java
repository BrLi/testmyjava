package Hero;

import static java.lang.System.out;

/**
 * @author brli
 *
 */
public class Commoner {

	private String name;

	public void greet() {
		out.println("Hi! I'm " + this.name + ". I don't have any super power.\n");
	}

	/**
	 * a dummy class
	 */
	public Commoner(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}

}
