package Hero;

public class Warrior extends Superhero {
	public String race;
	final String[] weapons = { "Fist", "Spear", "Sword", "Pike" };
	public int weaponNo;

	public Warrior(String sname, double shealth, double sdamage, double defense, String r, int wNo) {
		super(sname, shealth, sdamage, defense);
		this.race = r;
		this.weaponNo = wNo;
	}

	public void format() {
		System.out.printf("The name of the warrior is \" %s \"\n", this.getName());
		System.out.printf("Current HP is %.2f \n", this.getHealthPoint());
		System.out.printf("Damage: %.2f\n", this.getDamage());
		System.out.printf("Defense: %.2f\n", this.getDefense());
		System.out.printf("Using equipment: %s\n", this.getEquipment());
		System.out.printf("Using weapon: %s\n", this.getUsingWeaponName());
	}

	// ----Lab 1 & Lab 2----//
	public void setRace(String r) {
		this.race = r;
	}

	public String getRace() {
		return this.race;
	}

	public String getUsingWeaponName() {
		return weapons[weaponNo];
	}

}
