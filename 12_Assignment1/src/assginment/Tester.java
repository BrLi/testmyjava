package assginment;

/*
*
*/
public class Tester {

	public static void main(String[] args) {
		Company company = new Company("NCCU");
		
		company.addEmployee(new Employee("Simon", "Employee", 1200)); // Adding an employee "Simon" to company
		company.addEmployee(new Employee("Shan", "Employee", 1100)); // Adding an employee "Shan" to company
		company.addEmployee(new Manager("Wei", "Manager", 1500, 1.1)); // Adding an manager "Wei" to company
		
		company.addWorkDays("Simon", 5);
		company.addWorkDays("Simon", 5);
		company.addWorkDays("Simon", 5);
		company.addWorkDays("Simon", 5);
		company.overtimeWork("Simon", 3);
		company.overtimeWork("Simon", 1);
		company.overtimeWork("Simon", 1);
		
		company.addWorkDays("Shan", 15);;
		company.overtimeWork("Shan", 1);
		
		company.addWorkDays("Wei", 20);

		company.summarizeWage();
		company.analyzeWage();
		company.analyzeOvertime();
		
	}

}
