package assginment;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 */

/**
 * @author brli
 *
 */
public class WageAnalyzer implements Analyzer {
	private ArrayList<Employee> employees;
	private ArrayList<Integer> wageList = new ArrayList<Integer>();

	public ArrayList<Integer> WageList() {
		int i = 0; // index ptr
		wageList.clear();
		for(i = 0; i < employees.size(); i++) {
			Employee e = employees.get(i);
			wageList.add(e.getDailyWage()*e.getWorkDay());
		}
		return wageList;
	}

	/**
	 * 
	 */
	public WageAnalyzer(ArrayList<Employee> employeelist) {
		this.employees = employeelist;
	}

	@Override
	public void summary() {
		out.println("Wage\nSum: " + sum()
				  + "\nAvg: " + avg()
				  + "\nCount: " + count()
				  + "\nMax: " + max()
				  + "\nMin: " + min());
		out.println("");
	}

	@Override
	public int count() {
		return employees.size();
	}

	@Override
	public int sum() {
		int i = 0; // index ptr
		int s = 0; // sum
		while(i < employees.size()) {
			Employee e = employees.get(i);
			s += e.getWorkDay()*e.getDailyWage();
			i++;
		}
		return s;
	}

	@Override
	public int avg() {
		return sum()/employees.size();
	}

	@Override
	public int max() {
		return (int)Collections.max(WageList());
	}

	@Override
	public int min() {
		return (int)Collections.min(WageList());
	}

}
