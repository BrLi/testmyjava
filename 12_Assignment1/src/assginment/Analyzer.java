/**
 * 
 */
package assginment;

/**
 * @author brli
 *
 */
public interface Analyzer {
	void summary();
	int count();
	int sum();
	int avg();
	int max();
	int min();
}
