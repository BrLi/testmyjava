package assginment;

/**
 * 
 */

/**
 * @author brli
 *
 */
public class Manager extends Employee {
	private double bonusRate;
	
	public int payment() {
		double wage;
		wage = getDailyWage() * getWorkDay() * this.bonusRate + getOvertime() * 150;
		return (int) Math.round(wage);
	}
	/**
	 * 
	 */
	public Manager(String name,
				   String title,
				   int dailyWage,
				   double rate) {
		super(name, title, dailyWage);
		this.bonusRate = rate;
	}

}
