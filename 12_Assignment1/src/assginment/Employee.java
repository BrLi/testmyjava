package assginment;

/**
 * 
 */

/**
 * @author brli
 *
 */
public class Employee {
	
	private String name;
	private String title;
	private int dailyWage;
	private int workDay;
	private int overtimeCount;
	private int overtime;
	
	public int payment() {
		int wage = 0;
		wage = this.dailyWage * this.workDay + this.overtime * 150;
		return wage;
	}
	
	public void addWorkDays(int days) {
		this.workDay += days;
	}
	
	public void overtimeWork(int hour) {
		this.overtime += hour;
		this.overtimeCount += 1;
	}
	
	/**
	 * getter functions
	 */
	
	public String getName() {
		return this.name;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public int getDailyWage() {
		return this.dailyWage;
	}
	
	public int getWorkDay() {
		return this.workDay;
	}
	
	public int getOvertimeCount() {
		return this.overtimeCount;
	}
	
	public int getOvertime() {
		return this.overtime;
	}
	
	/**
	 * set function
	 */
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDailyWage(int wage) {
		this.dailyWage = wage;
	}
	
	public void setWorkDay(int day) {
		this.workDay = day;
	}
	
	public void setOvertime(int hour) {
		this.overtime = hour;
	}
	
	public void addOvertimeCount(int c) {
		this.overtimeCount += c;
	}

	/**
	 * 
	 */
	public Employee(String name, String title, int dailyWage) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.title = title;
		this.dailyWage = dailyWage;
		this.workDay = 0;
		this.overtimeCount = 0;
		this.overtime = 0;
	}
}
