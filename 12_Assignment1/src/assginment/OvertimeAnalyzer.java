package assginment;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 */

/**
 * @author brli
 *
 */
public class OvertimeAnalyzer implements Analyzer {

	private ArrayList<Employee> employees;
	private ArrayList<Integer> overHour = new ArrayList<Integer>();

	public ArrayList<Integer> OverHourList() {
		int i = 0; // index ptr
		overHour.clear();
		for(i = 0; i < employees.size(); i++) {
			Employee e = employees.get(i);
			overHour.add(e.getOvertime());
		}
		return overHour;
	}

	/**
	 * 
	 */
	public OvertimeAnalyzer(ArrayList<Employee> employeelist) {
		this.employees = employeelist;
	}


	@Override
	public void summary() {
		out.println("Overtime\nSum: " + sum()
				  + "\nAvg: " + avg()
				  + "\nCount: " + count()
				  + "\nMax: " + max()
				  + "\nMin: " + min());
		out.println("");
	}


	@Override
	public int count() {
		int i = 0; // index ptr
		int j = 0; // counter
		while(i < employees.size()) {
			Employee e = employees.get(i);
			if(e.getOvertimeCount() != 0) {
				j++;
			}
			i++;
		}
		return j;
	}


	@Override
	public int sum() {
		int i = 0; // index ptr
		int s = 0; // sum
		while(i < employees.size()) {
			Employee e = employees.get(i);
			s += e.getOvertime();
			i++;
		}
		return s;
	}


	@Override
	public int avg() {
		return sum()/employees.size();
	}


	@Override
	public int max() {
		return (int)Collections.max(OverHourList());
	}


	@Override
	public int min() {
		return (int)Collections.min(OverHourList());
	}

}
