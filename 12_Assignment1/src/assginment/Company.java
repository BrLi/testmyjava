package assginment;

import java.util.ArrayList;

import static java.lang.System.out;

public class Company {

	private String name;
	private ArrayList<Employee> employees;
	private WageAnalyzer wageAnalyzer;
	private OvertimeAnalyzer overtimeAnalyzer;

	public void addEmployee(Employee e) {
		this.employees.add(e);
	}
	
	public Employee findEmployee(String name) {
		int i = 0; // index
		while(i < employees.size()) {
			if(employees.get(i).getName() == name) {
				return employees.get(i);
			}
			else {
				i++;
			}
		}
		return null; 
	}
	
	public void addWorkDays(String name, int days) {
		Employee e = findEmployee(name); // fetch obj from list
		int day = e.getWorkDay(); // get current workday
		e.setWorkDay(day + days); // add addition days to workday
	}

	public void overtimeWork(String name, int hours) {
		Employee e = findEmployee(name);
		int hour = e.getOvertime(); // get incremented overtime hours (unit: hour)
		e.setOvertime(hour + hours);
		e.addOvertimeCount(1);
	}
	
	public void summarizeWage() {
		int i = 0; // index ptr
		out.println(this.name);
		while(i < employees.size()) {
			Employee e = employees.get(i);
			out.println("Name: " + e.getName() +
						"\nTitle: " + e.getTitle() + 
						"\nWorkDay: " + e.getWorkDay() + 
						"\nDaily Wage: " + e.getDailyWage() +
						"\nOvertime: " + e.getOvertime() + 
						"\nOvertime Count: " + e.getOvertimeCount());
			i++;
		}
		out.println("");
	}
	
	public void analyzeWage() {
		this.wageAnalyzer = new WageAnalyzer(employees);
		wageAnalyzer.summary();
	};
	
	public void analyzeOvertime() {
		this.overtimeAnalyzer = new OvertimeAnalyzer(employees);
		overtimeAnalyzer.summary();
	}
	
	public Company(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.employees = new ArrayList<Employee>();
	}

}
